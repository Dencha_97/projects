# Проекты собранные для портфолио

## Папка ASP.NET CORE
Представлены проекты реализованые на платформе ASP.NET CORE:
+ Парсинг из xml-файла в csv-файл. Реализован по шаблону MVC;
+ Авторизация и регистрация. Реализован по щаблону MVVM используя технология ASP.NET Identity

## Папка C#(Console)
Представлены работы реализованые на платформе .NET CORE. 
Задачи брал из вебинаров, интенсивов и курса, проходивший на Udemy (https://www.udemy.com/course/csharp-ru/) 

## Frontend (HTML, CSS, JS)
Представлен проект, который реализован на стеке (HTML, CSS, JS).
