﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ParsingMVC.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ParsingMVC.Controllers
{
	[AllowAnonymous]
	public class HomeController : Controller
	{
		InfoModelDto _infoModelDto = new InfoModelDto();

		public IActionResult Index() => View(_infoModelDto);

		[HttpPost]
		public async Task<IActionResult> Load(IFormFileCollection virtualFileCollection)
		{
			//Удаление всех сохраненных файлов csv
			DeleteFiles(@".\wwwroot\files\save\");

			if (virtualFileCollection != null && virtualFileCollection.Count() != 0)
			{
				foreach (var @virtual in virtualFileCollection.Select((file, index) => (file, index)))
				{
					try
					{
						string path = $@".\wwwroot\files\copy\{@virtual.file.FileName}";

						using (FileStream fs = new FileStream(path, FileMode.Create))
						{
							await @virtual.file.CopyToAsync(fs);
						}

						_infoModelDto.ListResult.Add($@".\wwwroot\files\copy\{@virtual.file.FileName}");
						_infoModelDto.FullMessange = null;
						_infoModelDto.CheckGoodLink = false;
						_infoModelDto.CheckGoodDiv = true;
						_infoModelDto.CheckGoodTable = true;
					}
					catch (Exception ex)
					{
						_infoModelDto.Messange = "Что то пошло не так";
						_infoModelDto.FullMessange = ex.Message;
						_infoModelDto.CheckGoodLink = false;
						_infoModelDto.CheckGoodDiv = false;
						_infoModelDto.CheckGoodTable = false;

						return View("Index", _infoModelDto);
					}
				}

				if (_infoModelDto.ListResult != null)
					_infoModelDto.Messange = "Успешно скопировано! Конвертируй!";
			}

			return View("Index", _infoModelDto);
		}

		public IActionResult Converter(InfoModelDto infoModelDto, List<string> checkColumns)
		{			
			var files = new DirectoryInfo(@".\wwwroot\files\copy\").GetFiles();

			//List<CSVColumns> list = new List<CSVColumns>();	

			if (files.Length != 0) 
			{
				//foreach (var file in infoModelDto.ListResult.Select((path, index) => (path, index)))
				foreach(var file in files)
				{
					try
					{
						using (FileStream fs = new FileStream(file.FullName, FileMode.Open))
						{
							var XMLColumns = XMLParsing(fs);
							var CSVColumns = InitialCSVColumns(XMLColumns);

							var listNameColumnCSV = NameColumnCSV(CSVColumns);
							var listValueColumnCSV = ValueColumnCSV(XMLColumns);

							int index = file.Name.LastIndexOf(".xml");

							string savePath = $@".\wwwroot\files\save\{file.Name.Replace(".xml", ".csv")}";

							using (FileStream fsw = new FileStream(savePath, FileMode.Create))
							{
								using (StreamWriter sw = new StreamWriter(fsw, Encoding.UTF8))
								{
									foreach (var item in listNameColumnCSV)
									{
										sw.Write($"{item} {infoModelDto.Delimiter} ");
									}
									sw.WriteLine();
									foreach (var item in listValueColumnCSV)
									{

										sw.Write($"{item} {infoModelDto.Delimiter} ");
									}

									infoModelDto.Messange = "Успешно сконвертировано! Скачивай!";
									infoModelDto.CheckGoodLink = true;
									infoModelDto.FullMessange = null;
									infoModelDto.CheckGoodTable = false;
								}
							}
						}

						infoModelDto.ListDownLink = GetSaveFiles(@".\wwwroot\files\save\");
						
					}
					catch (Exception ex)
					{
						infoModelDto.Messange = "Что то пошло не так";
						infoModelDto.FullMessange = ex.Message;
						infoModelDto.CheckGoodLink = false;
						infoModelDto.CheckGoodDiv = false;

						//Удаление всех сохраненных файлов xml
						DeleteFiles(@".\wwwroot\files\copy\");

						return View("Index", infoModelDto);
					}
				}
			}

			//Удаление всех сохраненных файлов xml
			DeleteFiles(@".\wwwroot\files\copy\");

			return View("Index", infoModelDto);
		}

		private void DeleteFiles(string path)
		{
			var files = new DirectoryInfo(path).GetFiles();
			foreach (var item in files)
			{
				item.Delete();
			}
		}

		private static string GetData(XDocument xdoc, string name) => xdoc.Descendants().FirstOrDefault(x => x.Name.LocalName == name)?.Value ?? "\"No Data\"";

		private static string GetDataDescendants(XDocument xdoc, string name)
		{
			var elements = xdoc.Descendants().FirstOrDefault(x => x.Name.LocalName == name);

			if (elements == null)
				return null;

			var descendantNodes = elements.DescendantNodesAndSelf().Where(c => c.NodeType == XmlNodeType.Text).Select(s => s.ToString()).ToArray();
			var str = descendantNodes.Aggregate((s1, s2) => $"{s1}, {s2}");

			return str;
		}

		private XMLModel XMLParsing(FileStream fs)
		{
			XDocument xdoc = XDocument.Load(fs);

			string cadNumber = GetData(xdoc, "cad_number");
			string category = GetDataDescendants(xdoc, "category");
			string vri = GetData(xdoc, "by_document");
			string readableAdress = GetData(xdoc, "readable_address");
			string birthDate = GetData(xdoc, "birth_date");
			string holderAdress = GetData(xdoc, "mailing_addess");
			string email = GetData(xdoc, "email");
			string snils = GetData(xdoc, "snils");
			string identityDoc = GetDataDescendants(xdoc, "identity_doc");
			string restrictctionsEncumbrancesData = GetDataDescendants(xdoc, "restrictions_encumbrances_data");
			string shareDescription = GetData(xdoc, "share_description");
			string surname = GetData(xdoc, "surname");
			string name = GetData(xdoc, "name");
			string patronymic = GetData(xdoc, "patronymic");
			string value26 = GetData(xdoc, "value26");
			string rightHolder = GetData(xdoc, "right_holder");
			string rightNumber = GetData(xdoc, "right_number");
			string registrationDate2 = GetData(xdoc, "registration_date2");
			string resident = GetData(xdoc, "resident");

			var colsDataXNLModel = new XMLModel
			{
				CadNumber = cadNumber,
				Category = category,
				Vri = vri,
				ReadableAdress = readableAdress,
				BirthDate = birthDate,
				HolderAdress = holderAdress,
				Email = email,
				Snils = snils,
				IdentityDoc = identityDoc,
				RestrictctionsEncumbrancesData = restrictctionsEncumbrancesData,
				ShareDescription = shareDescription,
				Surname = surname,
				Name = name,
				Patronymic = patronymic,
				Value26 = value26,
				RightHolder = rightHolder,
				RightNumber = rightNumber,
				RegistrationDate2 = registrationDate2,
				Resident = resident
			};

			return colsDataXNLModel;
		}

		private CSVColumns InitialCSVColumns(XMLModel model)
		{
			return new CSVColumns
			{
				CadNumber = model.CadNumber,
				Category = model.Category,
				Vri = model.Vri,
				Address = model.ReadableAdress,
				CopyrightHolderFIO = model.Fio,
				RightNumber = model.RightNumber,
				BirthDate = model.BirthDate,
				CopyrightHolderAddress = model.HolderAdress,
				Email = model.Email,
				CopyrightHolderDetails = model.IdentityDoc,
				Snils = model.Snils,
				RestrictionOfRights = model.RestrictctionsEncumbrancesData,
				Lessee = model.Resident
			};
		}

		private List<string> NameColumnCSV(CSVColumns columns)
		{
			List<string> list = new List<string>();

			list.Add(nameof(columns.CadNumber));
			list.Add(nameof(columns.Category));
			list.Add(nameof(columns.Vri));
			list.Add(nameof(columns.Address));
			list.Add(nameof(columns.CopyrightHolderFIO));
			list.Add(nameof(columns.RightNumber));
			list.Add(nameof(columns.BirthDate));
			list.Add(nameof(columns.CopyrightHolderAddress));
			list.Add(nameof(columns.Email));
			list.Add(nameof(columns.CopyrightHolderDetails));
			list.Add(nameof(columns.Snils));
			list.Add(nameof(columns.RestrictionOfRights));
			list.Add(nameof(columns.Lessee));

			return list;
		}

		private List<string> ValueColumnCSV(XMLModel model)
		{
			CSVColumns columns = new CSVColumns();
			List<string> list = new List<string>();

			list.Add(columns.CadNumber = model.CadNumber);

			list.Add(columns.Category = model.Category);
			list.Add(columns.Vri = model.Vri);
			list.Add(columns.Address = model.ReadableAdress);
			list.Add(columns.CopyrightHolderFIO = model.Fio);
			list.Add(columns.RightNumber = model.RightNumber);
			list.Add(columns.BirthDate = model.BirthDate);
			list.Add(columns.CopyrightHolderAddress = model.HolderAdress);
			list.Add(columns.Email = model.Email);
			list.Add(columns.CopyrightHolderDetails = model.IdentityDoc);
			list.Add(columns.Snils = model.Snils);
			list.Add(columns.RestrictionOfRights = model.RestrictctionsEncumbrancesData);
			list.Add(columns.Lessee = model.Resident);

			return list;
		}

		private List<string> GetSaveFiles(string path)
		{
			var list = new List<string>();
			var files = new DirectoryInfo(path).GetFiles();
			
			foreach (var file in files)
			{
				list.Add($@"\files\save\{file.Name}");
			}

			return list;
		}
	}
}
