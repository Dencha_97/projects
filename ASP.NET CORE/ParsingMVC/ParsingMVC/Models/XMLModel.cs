﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParsingMVC.Models
{
	public class XMLModel
	{
		public string CadNumber { get; set; }
		public string Category { get; set; }
		public string Vri { get; set; }
		public string ReadableAdress { get; set; }
		public string Fio => $"{Surname} {Name} {Patronymic}";
		public string RightInfo { get; set; }
		public string BirthDate { get; set; }
		public string HolderAdress { get; set; }
		public string Email { get; set; }
		public string Snils { get; set; }
		public string IdentityDoc { get; set; }
		public string RestrictctionsEncumbrancesData { get; set; }
		public string Holder { get; set; }
		public string ShareDescription { get; set; }
		public string Surname { get; set; }
		public string Name { get; set; }
		public string Patronymic { get; set; }
		public string Value26 { get; set; }
		public string RightHolder { get; set; }
		public string RightNumber { get; set; }
		public string RegistrationDate2 { get; set; }
		public string Resident { get; set; }
	}
}
