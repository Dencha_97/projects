using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Test_Authorization.Data;
using Test_Authorization.Models;

namespace Test_Authorization.Pages.Account
{
	public class RegisterModel : PageModel
	{
		private readonly UserManager<User> _userManager;
		private readonly SignInManager<User> _signInManager;

		public RegisterModel(UserManager<User> userManager, SignInManager<User> signInManager)
		{
			_userManager = userManager;
			_signInManager = signInManager;
		}

		[BindProperty]
		[Required]
		[Display(Name = "���")]
		public string R_Username { get; set; }

		[BindProperty]
		[Required]
		[Display(Name = "Email")]
		public string R_Email { get; set; }

		[BindProperty]
		[Required]
		[Display(Name = "�����")]
		public string R_Login { get; set; }

		[BindProperty]
		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "������")]
		public string R_Password { get; set; }

		[BindProperty]
		[Required]
		[Compare("R_Password", ErrorMessage = "������ �� ���������")]
		[DataType(DataType.Password)]
		[Display(Name = "����������� ������")]
		public string R_PasswordConfirm { get; set; }

		public IActionResult OnGet()
		{
			return Page();
		}

		public async Task<IActionResult> OnPostAsync()
		{
			if (ModelState.IsValid)
			{
				//PasswordHasher<IdentityUser> ph = new PasswordHasher<IdentityUser>();
				User user = new User { UserName = R_Username, Email = R_Email, Login = R_Login};


				var result = await _userManager.CreateAsync(user, R_Password);
				string path = @"D:\repository\gitlab\Test_Authorization\Test_Authorization\Data.json";

				if (result.Succeeded)
				{
					try
					{
						using (FileStream fs = System.IO.File.Create(path)) { }

						using (StreamWriter sw = new StreamWriter(path, true, Encoding.UTF8))
						{
							sw.WriteLine("{");
							sw.WriteLine($"\"Username\": \"{user.UserName}\",");
							sw.WriteLine($"\"Email\": \"{user.Email}\",");
							sw.WriteLine($"\"Login\": \"{user.Login}\",");
							sw.WriteLine($"\"Password\": \"{user.PasswordHash}\"");
							sw.WriteLine("}");
						}

					}
					catch (Exception ex)
					{
						Console.WriteLine(ex.Message);

					}

					//install cookie
					await _signInManager.SignInAsync(user, false); 
					return RedirectToPage("/Index");
				}
				else
				{
					foreach (var error in result.Errors)
					{
						ModelState.AddModelError(string.Empty, error.Description); 
					}
				}
			}
			return Page();
		}
	}


}
