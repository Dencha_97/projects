﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuessNumber
{
	class Computer
	{
		//диапазон чисел
		private int[] _allNumbers = new int[100];
		//попытки
		public int Attempt { get; private set; } 
		public Computer()
		{
			Attempt = 10;

			for (int i = 0; i < _allNumbers.Length; i++)
			{
				_allNumbers[i] = i;
			}
		}

		public void Search()
		{
			int number;
			int left = _allNumbers[0];
			int right = _allNumbers.Length;
			
			do
			{
				Console.Write("Загадай число: ");
				number = Convert.ToInt32(Console.ReadLine());
				
				while (Attempt > 0)
				{
					Console.WriteLine($"Осталось попыток: {Attempt}");
					int mid = (left + right) / 2;

					if (number > mid)
					{
						Attempt--;
						left = _allNumbers[mid];
						right = _allNumbers.Length;
					}
					else if (number < mid)
					{
						Attempt--;
						left = _allNumbers[0];
						right = _allNumbers[mid];
					}
					else if (number == mid)
					{
						Console.WriteLine($"\nКомпьютер, угадал число! \n");
						Attempt = 10;
						left = _allNumbers[0];
						right = _allNumbers.Length;
						break;
					}
				} 
				
				if (Attempt == 0)
				{
					Console.WriteLine("\nКомпьютер, проиграл.\n" +
									  "Загадай железяке попроще \n ");
					Attempt = 10;
					left = _allNumbers[0];
					right = _allNumbers.Length;
				}
				else
				{
					break;
				}

			} while (true);

		}
	}
}
