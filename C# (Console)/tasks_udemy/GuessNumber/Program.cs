﻿using System;
using System.Threading;
using System.Threading.Channels;

namespace GuessNumber
{
	class Program
	{
		static void Main(string[] args)
		{
			int mode; //режим работы игры
			int number; //загаданное число
			Player player = new Player();
			Computer computerPlayer = new Computer();
			Random random = new Random();
			Console.WriteLine("---------------- Игра --------------");
			Console.WriteLine("----------- Угадай число -----------\n");
			Console.WriteLine("В игре есть 2 режима работы: \n" +
			                  "Цифра 1: Игрок загадывает, компьютер отгадывает \n" +
							  "Цифра 2: Компьютер загадывает, игрок отгадывает \n");

			do
			{
				Console.Write("Выбери режим работы: ");
				mode = Convert.ToInt32(Console.ReadLine());

				switch (mode)
				{
					case 1:
						
						computerPlayer.Search();
						break;
					case 2:
						number = random.Next(100);
						player.Search(number);
						break;
					default:
						Console.WriteLine("Не выбран режим или не правильно указана цифра\n");
						break;
				}
			} while (true);
			
		}
	}
}
