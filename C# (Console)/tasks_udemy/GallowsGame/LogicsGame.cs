﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace GallowsGame
{
	class LogicsGame
	{
		private readonly int allowedMisses; //промахи при угадывании слова
		private bool[] openIndex; //проверка на открытые буквы
		private int triesCounter = 0; //счетчик попыток
		private string triedLetters; //используемые буквы

		//статус игры
		public GameStatus GameStatus { get; private set; } = GameStatus.NotStarted; //default

		//сгенирированное слово
		public string Word { get; private set; }

		//свойства для заполнения уже использованнных букв
		public string TriedLetters
		{
			get
			{
				var chars = triedLetters.ToCharArray();
				Array.Sort(chars);
				return new string(chars) ;
			}
		}

		//счетчик попыток
		public int RemainingTries
		{
			get
			{
				return allowedMisses - triesCounter;
			}
		}

		//инициализация промохов
		public LogicsGame(int allowedMisses = 6)
		{
			//ограничения по попытке передать кол-во промохов
			if (allowedMisses < 5 || allowedMisses > 8)
			{
				throw new ArgumentException("Количество допустимых промахов должно быть от 5 до 8.");
			}
			this.allowedMisses = allowedMisses;
		}

		/// <summary>
		/// генерит слово
		/// </summary>
		/// <returns></returns>
		public string GenerateWord()
		{
			string[] words = File.ReadAllLines("WordsStockRus.txt");
			Random r = new Random(DateTime.Now.Millisecond); //повысить рандомизацию

			int randomIndex = r.Next(words.Length - 1);

			Word = words[randomIndex];

			openIndex = new bool[Word.Length]; // по defualt false так как сначала буквы скрыты

			GameStatus = GameStatus.InProgress;

			return Word;
		}

		/// <summary>
		/// логика угадывания слова
		/// </summary>
		/// <param name="letter">буква</param>
		/// <returns></returns>
		public string GetLetter(char letter)
		{
			if (triesCounter == allowedMisses)
			{
				throw new InvalidOperationException($"Превышено максимальное количество промахов: {allowedMisses}");
			}

			if (GameStatus != GameStatus.InProgress)
			{
				throw new InvalidOperationException($"Несоответствующий статус игры: {GameStatus}");
			}

			bool openAny = false; //открытые буквы

			string result = string.Empty;
			for (int i = 0; i < Word.Length; i++)
			{
				if (Word[i] == letter)
				{
					openIndex[i] = true;
					openAny = true;
				}

				if (openIndex[i])
				{
					result += Word[i];
				}
				else
				{
					result += "-";
				}
			}

			//ничего не открыто
			if (!openAny)
				triesCounter++;

			//букву записать, которую использовали
			triedLetters += letter;

			if (IsWin())
			{
				GameStatus = GameStatus.Win;
			}
			else if (triesCounter == allowedMisses)
			{
				GameStatus = GameStatus.Lost;
			}

			return result;
		}

		/// <summary>
		/// определение победы
		/// </summary>
		/// <returns></returns>
		private bool IsWin()
		{
			foreach (var cur in openIndex)
			{
				if (cur == false)
				{
					return false;
				}
			}
			return true;
		}
	}
}
