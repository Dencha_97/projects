﻿namespace GallowsGame
{
	public enum GameStatus
	{
		Win,
		Lost,
		InProgress,
		NotStarted
	}
}