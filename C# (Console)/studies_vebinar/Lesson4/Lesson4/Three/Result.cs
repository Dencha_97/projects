﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4.Three
{
	class Result
	{
		static public void Method()
		{
			double n1, n2;
			char sign;
			List<string> list = new List<string>();
			Calculator calc;
			char action;

			do
			{
				Console.Write("Выбирете '=' для вычисление или 'h' для вывода история: ");
				action = Convert.ToChar(Console.ReadLine());

				switch (action)
				{
					case '=':
						try
						{
							Console.Write("Введите первое число: ");
							n1 = Convert.ToInt32(Console.ReadLine());
							Console.Write("Введите второе число: ");
							n2 = Convert.ToInt32(Console.ReadLine());

							calc = new Calculator(n1, n2);

							Console.Write("Введите знак: ");
							sign = Convert.ToChar(Console.ReadLine());

							calc.Operation(sign);
							list.Add(n1 + " + " + n2 + " = " + calc.Operation(sign));
							Console.WriteLine(n1 + " + " + n2 + " = " + calc.Operation(sign));

						}
						catch (Exception e)
						{
							Console.WriteLine(e.Message);
						}

						Console.WriteLine();
						break;
					case 'h':
						foreach (var item in list)
						{
							Console.WriteLine(item);
						}
						break;
					default:
						Console.WriteLine("Неверно введено");
						break;
				}				
			} while (true);
		}
	}
}