﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4.Three
{
	/*
	 3. Реализовать программу “Калькулятор” в стиле ООП. В
		калькуляторе предусмотреть функцию сохранения всех
		промежуточных результатов в процессе вычисления (историю), с
		возможностью вывода на экран.
		Примечание: класс “Калькулятор”.
	 */
	class Calculator
	{
		double a;
		double b;
		double result;
		

		public Calculator(double n1, double n2)
		{
			a = n1;
			b = n2;
		}

		public double Operation(char sign)
		{
			switch (sign)
			{
				case '+':
					return a + b;
					break;
				case '-':
					return a - b;
					break;
				case '/':
					return  a / b;
					break;
				case '*':
					return a * b;
					break;
				default:
					return 0;
					break;
			}					
		}


	}
}
