﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4.Two
{
	//класс Сотрудник
	public class Employee : Employees
	{
		public string Name { get; }
		public string LastName { get; }
		public int Age { get; }
		public string Position { get; } //должность
		public int Salary { get; set; } //зарплатa

		public Employee() { }
		public Employee(string name, string lname, int age, string pos, int salary)
		{
			this.Name = name;
			this.LastName = lname;
			this.Age = age;
			this.Position = pos;
			this.Salary = salary;
		}

		public void Show()
		{
			Console.WriteLine("Имя: " + this.Name +
								"\nФамилия: " + this.LastName +
								"\nВозраст: " + this.Age +
								"\nДолжность: " + this.Position +
								"\nЗарплата: " + this.Salary +"\n");
		}
	}
}
