﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4.Two
{
	//класс Сотрудники
	public class Employees
	{
		public Employee Employee { get; set; }

		public Employees()
		{
			
		}

		//Средняя зарплата
		public int AsrSalary(params Employee[] ob)
		{
			int count = 0;
			int sum = 0;
			for (int i = 0; i < ob.Length; i++)
			{
				sum += ob[i].Salary;
				count++;
			}
			return sum / count;
		}

		//Максимальная зарплата
		public int MaxSalary(params Employee[] ob)
		{
			var max = ob[0];
			for (int i = 0; i < ob.Length; i++)
			{
				if (max.Salary < ob[i].Salary)
					max = ob[i];
			}
			return max.Salary;
		}

		//Минимальная зарплата
		public int MinSalary(params Employee[] ob)
		{
			var min = ob[0];
			for (int i = 0; i < ob.Length; i++)
			{
				if (min.Salary > ob[i].Salary)
					min = ob[i];
			}
			return min.Salary;
		}
	}
}
