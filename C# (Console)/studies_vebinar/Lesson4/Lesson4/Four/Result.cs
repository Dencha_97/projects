﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4.Four
{
	class Result
	{
		public static void Method()
		{
			Roulette roulette = new Roulette(100);

			int numberOut;
			Color colorOut;
			string colorInfo; 

			do
			{
				Console.Write("Сделайте ставку: ");
				roulette.Stavka = Convert.ToInt32(Console.ReadLine());
				Console.Write("Красная или черная: ");
				string colorStavka = Console.ReadLine();
				roulette.Color = "черная" == colorStavka ? Color.black : Color.red;

				if (roulette.ProcessGame(roulette.Stavka, roulette.Color, out numberOut, out colorOut))
				{
					colorInfo = colorOut == Color.red ? "красная" : "черная";
					Console.WriteLine($"Ставка сыграла! Выпала {numberOut} {colorInfo}");
					Console.WriteLine($"Ваш кеш: {roulette.CashHuman}. Кеш казино: {roulette.CashCasino}");
					if (roulette.IsWin())
					{
						Console.WriteLine("Ты разорил казино! Поздравляю, Ты выйграл");
						break;
					}
				}
				else
				{
					colorInfo = colorOut == Color.red ? "красная" : "черная";
					Console.WriteLine($"Выпала {numberOut} {colorInfo}");
					Console.WriteLine($"Ваш кеш: {roulette.CashHuman}. Кеш казино: {roulette.CashCasino}");
					if(roulette.CashHuman < 0)
						Console.WriteLine("Ты програл все!!!");
				}
				Console.WriteLine();
			} while (roulette.CashHuman > 0);
		}
	}
}
