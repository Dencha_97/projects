﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4.Four
{
	/*
	 4. Игра “Рулетка”. Игрок обладает некоторой начальной суммой и
		делает ставку от 0 до 36, или на красное, на черное. В зависимости от
		выпавшего значения, его сумма либо удваивается, либо увеличивается
		в 36 раз, либо переходит в Казино. Казино тоже может разориться.
		Игра заканчивается, когда разоряется либо казино, либо игрок.
		Примечание: использовать класс Random.
	 */


	class Roulette
	{
		public int CashHuman { get; set; }
		public int CashCasino { get; private set; } = 100;
		public int Stavka { get; set; }
		public Color Color { get; set; }

		public Roulette(int cash)
		{
			CashHuman = cash;
		}

		public bool ProcessGame(int stavka, Color colorIn, out int numberOut, out Color colorOut)
		{
			Stavka = stavka;
			Color = colorIn;

			Random r = new Random();
			int numberNow = r.Next(0, 36);
			int colorNow = r.Next(1, 3);

			if(numberNow == Stavka && colorNow == (int)Color)
			{
				CashCasino -= CashHuman;
				CashHuman += CashHuman;
				numberOut = numberNow;
				colorOut = (Color)colorNow;
				return true;
			}
			else
			{
				CashCasino += Stavka;
				CashHuman -= Stavka;
				numberOut = numberNow;
				colorOut = (Color)colorNow;
				return false;
			}		
		}

		public bool IsWin()
		{
			if (CashCasino < 0)
				return true;

			return false;
		}
	}
}
