﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2
{
	class Three
	{
		//3.Реализовать программу “Калькулятор счастья”.
		//Есть фиксированный процент по вкладу в месяц.
		//Пользователь вводит ту сумму, которую он хочет вложить и какую он хотел бы получить.
		//В результате Система отображает значение сколько
		//месяцев(или дней) потребуется ожидать роста вклада до желаемого значения.
		public static void Method()
		{
			const float procent = 9.5f; //процент по вкладу в месяц
			double sum;    //сумма, которую вложить
			double sumDesired; //желаемая сумма
			int result = 0;

			Console.Write("Введите сумму вклада: ");
			sum = Convert.ToDouble(Console.ReadLine());
			Console.Write("Введите желаемую сумму: ");
			sumDesired = Convert.ToDouble(Console.ReadLine());

			result = (int)(sumDesired / (((sum * 10) / 100) + sum));

			Console.WriteLine($"{result} месяц (-ев) потребуется, до получения этой суммы {sumDesired}");

		}
	}
}
